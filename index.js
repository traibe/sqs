const {AWS}                 = require('@traibe/aws');
const SQS                   = new AWS.SQS({ apiVersion: "2012-11-05" });
const { Consumer }          = require('sqs-consumer');

async function listQueues(prefix){
    try{
        const params = {
            QueueNamePrefix: prefix
        };
        const queues    = await SQS.listQueues(params).promise();
        const urls      = (queues && queues.QueueUrls) || [];
        if(urls.length > 0) 
            return urls;
    }catch(err){
        console.log(err);
    }
    return Promise.reject(`No queues matching prefix '${prefix}'`);
}


async function listenToQueue(prefix, next){
    let queues = [];
    try{
        const urls = await listQueues(prefix) || [];

        for(let i = 0; i < urls.length; i++){
            try{
                console.log(`Creating SQS listener: ${prefix} / ${urls[i]}`)
                const app = Consumer.create({
                    queueUrl: urls[i],
                    handleMessage: async (message) => {
                        // do some work with `message`
                        let parsedMessage = JSON.parse(message.Body)
                        next(parsedMessage);
                    }
                });
                app.on('error', (err) => {
                    console.error(err.message);
                });
                app.on('processing_error', (err) => {
                    console.error(err.message);
                });
                app.start();

                // Add the queue url to the list of queues
                queues.push(urls[i]);
            }catch(err){
                console.log(err);
            }
        }
    }catch(err){
        console.log(err)
    }
    return queues;
}


module.exports = {
    SQS,
    listenToQueue
}